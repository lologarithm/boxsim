package quadtree

// some constants for tile-indeces, for clarity
const (
	topRightTile    = 0
	topLeftTile     = 1
	bottomLeftTile  = 2
	bottomRightTile = 3
)

// QuadTree is the core tree structure.
type QuadTree struct {
	root     qtile
	maxBoxes int // max boxes per qtile
}

// NewQuadTree constructs an empty quad-tree of the given size
func NewQuadTree(bbox BoundingBox) QuadTree {

	qt := QuadTree{
		maxBoxes: 16,
	}
	qt.root = qtile{BoundingBox: bbox, tree: &qt}
	return qt
}

// qtile is a single tile of the quadtree
type qtile struct {
	BoundingBox
	contents []BoundingBox // values stored in this tile
	level    int           // level this tile is at. root is level 0
	quads    [4]*qtile     // sub-tiles. none or four.
	tree     *QuadTree     // keep a ref back to top
}

// Add a value to the quad-tree by trickle down from the root node.
func (qb *QuadTree) Add(v BoundingBox) bool {
	return qb.root.add(v)
}

// Remove a value from the quad-tree by trickle down from the root node.
func (qb *QuadTree) Remove(v BoundingBox) bool {
	return qb.root.remove(v)
}

// Move a box to new box
func (qb *QuadTree) Move(newb BoundingBox, oldloc BoundingBox) {
	queue := make([]*qtile, 1, 16)
	queue[0] = &qb.root
	var ct *qtile
	var i int
	found := false
	for i = 0; i < len(queue); i++ {
		ct = queue[i]
		for vidx, v := range ct.contents {
			if newb.ID == v.ID {
				if ct.Contains(newb) {
					ct.contents[vidx] = newb
					return
				}
				found = true
				// remove from list of contents
				slen := len(ct.contents) - 1
				ct.contents[vidx] = ct.contents[slen]
				ct.contents = ct.contents[:slen]
				break
			}
		}
		if found {
			break
		}
		if ct.quads[topRightTile] != nil {
			for _, child := range ct.quads {
				if child.Contains(oldloc) {
					queue = append(queue, child)
				}
			}
			continue
		}
	}
	i--
	// iterate backwards back up the stack until we find a new home.
	// dont check 0 since that is root
	for ; i > 0; i-- {
		ct = queue[i]
		if ct.Intersects(newb) {
			ct.add(newb)
			return
		}
	}
	// We didn't find it
	qb.root.add(newb)
	return
}

// Clone will create a full copy of this quadtree.
func (qb *QuadTree) Clone() *QuadTree {
	nt := &QuadTree{}
	nt.root = qb.root.clone(nt)
	return nt
}

// Query will return all objects which intersect the query box
func (qb *QuadTree) Query(qbox BoundingBox) []BoundingBox {
	ret := []BoundingBox{}

	if !qb.root.Intersects(qbox) {
		// tile doesn't intersect, stop here.
		return ret
	}

	queue := make([]*qtile, 1, 16)
	queue[0] = &qb.root
	var ct *qtile
	for i := 0; i < len(queue); i++ {
		ct = queue[i]
		for _, v := range ct.contents {
			if v.Intersects(qbox) {
				ret = append(ret, v)
			}
		}
		if ct.quads[topRightTile] != nil {
			for _, child := range ct.quads {
				if child.Intersects(qbox) {
					queue = append(queue, child)
				}
			}
		}
	}
	return ret
}

func (tile qtile) clone(nt *QuadTree) qtile {
	ntile := qtile{
		BoundingBox: tile.BoundingBox,
		level:       tile.level,
		contents:    make([]BoundingBox, len(tile.contents)),
		quads:       [4]*qtile{},
		tree:        nt,
	}
	copy(ntile.contents, tile.contents)
	for idx, c := range tile.quads {
		if c != nil {
			cClone := c.clone(nt)
			ntile.quads[idx] = &cClone
		}
	}
	return ntile
}

func (tile *qtile) add(v BoundingBox) bool {
	queue := make([]*qtile, 1, 16)
	queue[0] = tile
	var ct *qtile
	var found bool
	for i := 0; i < len(queue); i++ {
		found = false
		ct = queue[i]
		if ct.quads[topRightTile] == nil {
			ct.insert(v)
			return true
		}
		for _, child := range ct.quads {
			if child.Contains(v) {
				// suitable sub-tile for value found
				// lets add that and keep diving down until we find a leaf.
				queue = append(queue, child)
				found = true
				break
			}
		}
		if !found {
			ct.insert(v)
			return true
		}
	}
	return false
}

// insert will add a box to the contents of the tile.
func (tile *qtile) insert(v BoundingBox) {
	tile.contents = append(tile.contents, v)

	// Check for too many boxes and no children qaudrants (leaf node)
	if len(tile.contents) > tile.tree.maxBoxes && tile.quads[0] == nil {
		// If tile is large enough to split, do it, otherwise... just keep stacking
		if tile.BoundingBox.MaxX-tile.BoundingBox.MinX > 8 && tile.BoundingBox.MaxY-tile.BoundingBox.MinY > 8 {
			tile.split()
		}
	}
}

// split the tile into 4 qauds and re-insert children into those new tiles.
func (tile *qtile) split() {
	midx := tile.MaxX/2.0 + tile.MinX/2.0
	midy := tile.MaxY/2.0 + tile.MinY/2.0

	tile.quads[topRightTile] = &qtile{
		BoundingBox: BoundingBox{ID: 0, MinX: midx, MaxX: tile.MaxX, MinY: midy, MaxY: tile.MaxY},
		level:       tile.level + 1,
		tree:        tile.tree,
	}
	tile.quads[topLeftTile] = &qtile{
		BoundingBox: BoundingBox{ID: 0, MinX: tile.MinX, MaxX: midx, MinY: midy, MaxY: tile.MaxY},
		level:       tile.level + 1,
		tree:        tile.tree,
	}
	tile.quads[bottomLeftTile] = &qtile{
		BoundingBox: BoundingBox{ID: 0, MinX: tile.MinX, MaxX: midx, MinY: tile.MinY, MaxY: midy},
		level:       tile.level + 1,
		tree:        tile.tree,
	}
	tile.quads[bottomRightTile] = &qtile{
		BoundingBox: BoundingBox{ID: 0, MinX: midx, MaxX: tile.MaxX, MinY: tile.MinY, MaxY: midy},
		level:       tile.level + 1,
		tree:        tile.tree,
	}

	tempList := tile.contents

	// clear values on this tile
	tile.contents = []BoundingBox{}

	// reinsert from parent slice
	for _, v := range tempList {
		tile.add(v)
	}
}

func (tile *qtile) remove(qbox BoundingBox) bool {
	// end recursion if this tile does not intersect the query range
	if !tile.Intersects(qbox) {
		return false
	}

	// see if we can find it in the contents
	for vidx, v := range tile.contents {
		if qbox.ID == v.ID {
			// REMOVE IT
			clen := len(tile.contents) - 1
			tile.contents[vidx] = tile.contents[clen]
			tile.contents = tile.contents[:clen]
			return true
		}
	}

	// if this tile didnt' contain it, recurse
	if tile.quads[topRightTile] != nil {
		for _, child := range tile.quads {
			if child.remove(qbox) {
				return true
			}
		}
	}

	return false
}
