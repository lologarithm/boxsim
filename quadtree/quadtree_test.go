package quadtree

import (
	"log"
	"math"
	"math/rand"
	"testing"
)

// Generates n BoundingBoxes in the range of frame with average width and height avgSize
func randomBoundingBoxes(n int, frame BoundingBox, avgSize float64) []BoundingBox {
	ret := make([]BoundingBox, n)

	for i := 0; i < len(ret); i++ {
		w := int32(rand.NormFloat64() * avgSize)
		h := int32(rand.NormFloat64() * avgSize)
		x := int32(rand.Float64()*float64(frame.MaxX-frame.MinX)) + frame.MinX
		y := int32(rand.Float64()*float64(frame.MaxY-frame.MinY)) + frame.MinY
		ret[i] = BoundingBox{
			ID:   uint32(i + 1),
			MinX: x,
			MaxX: int32(math.Min(float64(frame.MaxX), float64(x+w))),
			MinY: y,
			MaxY: int32(math.Min(float64(frame.MaxY), float64(y+h))),
		}
	}

	return ret
}

func findBox(boxes []BoundingBox, id uint32) int {
	for i, box := range boxes {
		if box.ID == id {
			return i
		}
	}
	return -1
}

// TestQueryQuadTree tests that queries find the same boxes as the
func TestQueryQuadTree(t *testing.T) {
	world := BoundingBox{ID: 0, MinX: -1000000, MaxX: 1000000, MinY: -1000000, MaxY: 1000000}
	rects := randomBoundingBoxes(1000, world, 5)
	qt := NewQuadTree(world)

	// Build up a space full of rectangles
	for _, v := range rects {
		qt.Add(v)
	}

	// Build up some queries
	queries := randomBoundingBoxes(1000, world, 10)

	for _, q := range queries {
		intersects := []BoundingBox{}
		for _, v := range rects {
			if q.Intersects(v) {
				intersects = append(intersects, v)
			}
		}

		queryResult := qt.Query(q)

		if len(intersects) != len(queryResult) {
			t.Errorf("intersects and queryResult differ in length:\n%#v\n%#v\n", intersects, queryResult)
		}
		for _, inter := range intersects {
			if findBox(queryResult, inter.ID) == -1 {
				t.Logf("Failed to find box: %#v in query results\n", inter)
				t.FailNow()
			}
		}
	}
}

type mockBox struct {
	BoundingBox
	ID uint32
}

func (mb mockBox) BoxID() uint32 {
	return mb.ID
}

func TestQuadRemove(t *testing.T) {
	world := BoundingBox{ID: 0, MinX: -1000000, MaxX: 1000000, MinY: -1000000, MaxY: 1000000}
	points := randomBoundingBoxes(10, world, 10)
	qt := NewQuadTree(world)
	for _, v := range points {
		qt.Add(v)
	}

	childs := qt.Query(world)
	if len(childs) != 10 {
		log.Printf("Failed to find all children in the world! expected: %d, actual: %d", 10, len(childs))
		t.FailNow()
	}

	found := qt.Remove(points[0])
	if !found {
		log.Printf("Box not found!? %d", points[0].ID)
	}
}

// Benchmark insertion into quad-tree
func BenchmarkInsert(b *testing.B) {
	b.StopTimer()
	world := BoundingBox{ID: 0, MinX: -10000000, MaxX: 10000000, MinY: -10000000, MaxY: 10000000}
	values := randomBoundingBoxes(b.N, world, 5)
	qt := NewQuadTree(world)

	b.StartTimer()
	for _, v := range values {
		qt.Add(v)
	}
}

// Benchmark quering the quad-tree on set of rectangles
// Execute b.N queries against one million rects in a 10 millon x 10 million space
func BenchmarkQuery(b *testing.B) {
	b.StopTimer()
	rand.Seed(1)
	world := BoundingBox{ID: 0, MinX: -10000000, MaxX: 10000000, MinY: -10000000, MaxY: 10000000}
	boxes := randomBoundingBoxes(1000000, world, 5)
	qt := NewQuadTree(world)
	for _, v := range boxes {
		qt.Add(v)
	}
	queries := randomBoundingBoxes(b.N, world, 10)

	b.StartTimer()
	for _, q := range queries {
		qt.Query(q)
	}
}

// BenchmarkBoxIntersects checks performance of the box intersects function (executing 10 queries agains one hundred rects per cycle)
func BenchmarkBoxIntersects(b *testing.B) {
	b.StopTimer()
	rand.Seed(1)
	world := BoundingBox{ID: 0, MinX: -1000000, MaxX: 1000000, MinY: -1000000, MaxY: 1000000}
	// Build one hundred boxes to query for intersection
	boxes := randomBoundingBoxes(100, world, 5)
	qs := randomBoundingBoxes(10, world, 10)
	b.StartTimer()
	var r bool
	for i := 0; i < b.N; i++ {
		for _, q := range qs {
			for _, v := range boxes {
				r = q.Intersects(v)
			}
		}
	}
	_ = r
}
