package quadtree

// BoundingBox is a box
type BoundingBox struct {
	ID   uint32
	MinX int32
	MaxX int32
	MinY int32
	MaxY int32
}

// Intersects returns true if o intersects
func (b BoundingBox) Intersects(o BoundingBox) bool {
	return b.MaxX >= o.MinX && b.MinX <= o.MaxX && b.MaxY >= o.MinY && b.MinY <= o.MaxY
}

// Contains returns true if o is within this
func (b BoundingBox) Contains(o BoundingBox) bool {
	return b.MinX <= o.MinX && b.MinY <= o.MinY && b.MaxX >= o.MaxX && b.MaxY >= o.MaxY
}
