package boxsim

import (
	"testing"
)

func TestMultVect(t *testing.T) {
	v1 := Vect2{2, 2}
	vmult := MultVect2(v1, 2)
	if vmult.X != 4 && vmult.Y != 4 {
		t.FailNow()
	}
}
