package boxsim

import (
	"fmt"
	"math/rand"
	"testing"
)

func TestTick(t *testing.T) {
	// 1. Create simple scene
	ss := NewSimulatedSpace(10000, 10000, 50)
	o1 := RigidBody{
		Velocity: Vect2{100, 100},
		Position: Vect2{0, 0},
		Force:    Vect2{0, 0},
	}
	ss.AddEntity(o1, false)
	// 2. Make sure single tick correctly ticks.
	for i := int32(1); i < 50; i++ {
		ss.Tick(false) // dont care about collisions, just tick.
		o1 = ss.entities[0]
		if o1.Position.X != i*(o1.Velocity.X/50) {
			fmt.Printf("Incorrect X position after physics update. Expected: %d Actual: %d\n", i*(o1.Velocity.X/50), o1.Position.X)
			t.FailNow()
		}
	}
	fmt.Println("Tick test passed.")
}

func BenchmarkTick(b *testing.B) {
	// 1,000 objects of size 50mx50m (ships?)
	// 9,000 objects of size 5mx5m (other mobile)
	// 50,000 objects of size 10cmx10cm (small immobile objects)
	// In a 5km area
	// 50 updates/sec
	ss := NewSimulatedSpace(2000000, 2000000, 50)
	ss.entities = make([]RigidBody, 50000)
	for i := uint32(0); i < 1000; i++ {
		o1 := RigidBody{
			ID:       i,
			Velocity: Vect2{int32(rand.Intn(100)), int32(rand.Intn(100))},
			Position: Vect2{int32(rand.Intn(500000)), int32(rand.Intn(500000))},
			Force:    Vect2{0, 0},
			Height:   5000,
			Width:    5000,
		}
		ss.AddEntity(o1, false)
	}
	for i := uint32(1000); i < 9000; i++ {
		o1 := RigidBody{
			ID:       i,
			Velocity: Vect2{int32(rand.Intn(100)), int32(rand.Intn(100))},
			Position: Vect2{int32(rand.Intn(500000)), int32(rand.Intn(500000))},
			Force:    Vect2{0, 0},
			Height:   500,
			Width:    500,
		}
		ss.AddEntity(o1, false)
	}
	for i := uint32(9000); i < 50000; i++ {
		o1 := RigidBody{
			ID: i,
			// No velocity
			Position: Vect2{int32(rand.Intn(500000)), int32(rand.Intn(500000))},
			Force:    Vect2{0, 0},
			Height:   10,
			Width:    10,
		}
		ss.AddEntity(o1, false)
	}

	b.ResetTimer()
	// 2. Make sure single tick correctly ticks.
	for i := 0; i < b.N; i++ {
		ss.Tick(true)
	}
}

func BenchmarkClone(b *testing.B) {
	// 1,000 objects of size 50mx50m (ships?)
	// 9,000 objects of size 5mx5m (other mobile)
	// 50,000 objects of size 10cmx10cm (small immobile objects)
	// In a 5km area
	ss := NewSimulatedSpace(2000000, 2000000, 50)
	ss.entities = make([]RigidBody, 50000)
	for i := uint32(0); i < 1000; i++ {
		o1 := RigidBody{
			ID:       i,
			Velocity: Vect2{int32(rand.Intn(100)), int32(rand.Intn(100))},
			Position: Vect2{int32(rand.Intn(500000)), int32(rand.Intn(500000))},
			Force:    Vect2{0, 0},
			Height:   5000,
			Width:    5000,
		}
		ss.AddEntity(o1, false)
	}
	for i := uint32(1000); i < 9000; i++ {
		o1 := RigidBody{
			ID:       i,
			Velocity: Vect2{int32(rand.Intn(100)), int32(rand.Intn(100))},
			Position: Vect2{int32(rand.Intn(500000)), int32(rand.Intn(500000))},
			Force:    Vect2{0, 0},
			Height:   500,
			Width:    500,
		}
		ss.AddEntity(o1, false)
	}
	for i := uint32(9000); i < 50000; i++ {
		o1 := RigidBody{
			ID: i,
			// No velocity
			Position: Vect2{int32(rand.Intn(500000)), int32(rand.Intn(500000))},
			Force:    Vect2{0, 0},
			Height:   10,
			Width:    10,
		}
		ss.AddEntity(o1, false)
	}

	b.ResetTimer()
	// 2. Make sure single tick correctly ticks.
	for i := 0; i < b.N; i++ {
		ss.tree.Clone()
	}
}
