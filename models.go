package boxsim

import (
	"fmt"
	"math"

	"gitlab.com/lologarithm/boxsim/quadtree"
)

// CrossProductVect2 returns cross product of a and b
func CrossProductVect2(a Vect2, b Vect2) int32 {
	return a.X*b.Y - a.Y*b.X
}

// CrossScalar returns the cross of the vect and scalar
func CrossScalar(v Vect2, s int32) Vect2 {
	return Vect2{v.Y * s, -s * v.X}
}

// MultVect2 retuns a multiplied by s
func MultVect2(a Vect2, s int32) Vect2 {
	return Vect2{a.X * s, a.Y * s}
}

// AngleVect2 returns the angle between a and b in radians
func AngleVect2(a Vect2, b Vect2) float64 {
	alpha := float64(a.X*a.X+a.Y*b.Y) / (a.Magnitude() * b.Magnitude())
	return math.Acos(alpha)
}

// AddVect2 will return the result of adding two vetors together
func AddVect2(v, v2 Vect2) Vect2 {
	return Vect2{v.X + v2.X, v.Y + v2.Y}
}

// SubVect2 will return the result of subtracting v2 from v
func SubVect2(v, v2 Vect2) Vect2 {
	return Vect2{v.X - v2.X, v.Y - v2.Y}
}

// NormalizeVect2 will normalize a vector to a given magnitude.
// If mag == 0 it will be normalized to magnitude max of 1.
func NormalizeVect2(a Vect2, mag int32) Vect2 {
	oldmag := float64(a.Magnitude())
	if int32(oldmag) == mag {
		return a
	}
	return Vect2{
		X: int32((float64(a.X) / oldmag) * float64(mag)),
		Y: int32((float64(a.Y) / oldmag) * float64(mag)),
	}
}

// RotateVect2 returns the result of rotating v by given radians around the origin
func RotateVect2(v Vect2, radians float64) Vect2 {
	result := Vect2{}
	result.X = int32(float64(v.X)*math.Cos(radians) - float64(v.Y)*math.Sin(radians))
	result.Y = int32(float64(v.X)*math.Sin(radians) + float64(v.Y)*math.Cos(radians))
	return result
}

// Vect2 represents a 2d vector
type Vect2 struct {
	X, Y int32
}

// Magnitude returns the magnitude of the vector
func (v Vect2) Magnitude() float64 {
	return math.Sqrt(float64(v.X*v.X + v.Y*v.Y))
}

// String returns string representation of the vector
func (v Vect2) String() string {
	return fmt.Sprintf("(%d,%d)", v.X, v.Y)
}

// RigidBody is a rigid box in the physics simulation centered on Position
type RigidBody struct {
	ID uint32 // Unique ID for this rigidbody

	Position Vect2 // coords x,y of center of entity  (arbitrary units)
	Velocity Vect2 // speed in vector format (units/sec)
	Force    Vect2 // Force to apply each tick.

	Angle           float64 // Current heading (radians)
	AngularVelocity float64 // speed of rotation around the Z axis (radians/sec)
	Torque          float64 // Torque to apply each tick

	Mass       int32 // Mass of the object, (kg)
	InvMass    int32 // Inverted mass for physics calcs
	Inertia    int32 // Inertia of the object
	InvInertia int32 // Inverted Inertia for physics calcs

	Height int32
	Width  int32

	bounds quadtree.BoundingBox // cached bounds
	dirty  bool
}

// RigidBounds returns the bounding box for the rigid body
func RigidBounds(rb RigidBody) quadtree.BoundingBox {
	// specially handle rotated boxes
	if rb.Angle != 0 && rb.Angle != math.Pi && rb.Angle != math.Pi*2 {
		s := math.Sin(rb.Angle)
		c := math.Cos(rb.Angle)
		if s < 0 {
			s = -s
		}
		if c < 0 {
			c = -c
		}
		wn := int32(float64(rb.Height)*s) + int32(float64(rb.Width)*c) // width of AABB
		hwn := wn / 2
		hn := int32(float64(rb.Height)*c) + int32(float64(rb.Width)*s) // height of AABB
		hhn := hn / 2
		return quadtree.BoundingBox{
			MinX: rb.Position.X - hwn,
			MaxX: rb.Position.X + hwn,
			MinY: rb.Position.Y - hhn,
			MaxY: rb.Position.Y + hhn,
		}
	}

	halfh := rb.Height / 2
	halfw := rb.Width / 2
	return quadtree.BoundingBox{
		MinX: rb.Position.X - halfw,
		MaxX: rb.Position.X + halfw,
		MinY: rb.Position.Y - halfh,
		MaxY: rb.Position.Y + halfh,
	}
}

// NewRigidBody just constructs a rigid body caching some useful values like invmass
func NewRigidBody(id uint32, h int32, w int32, pos Vect2, vel Vect2, angle float64, mass int32) RigidBody {
	return RigidBody{
		ID:       id,
		Position: pos,
		Velocity: vel,
		Angle:    angle,
		Mass:     mass,
		InvMass:  1 / mass,
		Height:   h,
		Width:    w,
	}
}

// PhysicsEntityUpdate message linked to an Entity.
type PhysicsEntityUpdate struct {
	UpdateType byte // See simulator.go physics constants
	ObjA       uint32
	ObjB       uint32
}
