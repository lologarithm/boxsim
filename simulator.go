package boxsim

import (
	"math"
	"sync"
	"time"

	"gitlab.com/lologarithm/boxsim/quadtree"
)

// Physics Constants
const (
	FullCircle = math.Pi * 2 // Useful for keeping angles between 0-360

	UnknownUpdate   = byte(0)
	AddEntity       = byte(1)
	RemoveEntity    = byte(2)
	UpdateForces    = byte(3)
	UpdatePosition  = byte(4)
	UpdateCollision = byte(5)
)

// Simulator design:
//  1. Needs to be able to represent position of each thing in time correctly.
//  2. Probably want a simplified 2d physics simulator running to allow for things with velocity?
//  3. Each tick should have an ID and should be rewindable (so we can insert updates in the past)
//  4. Notification of collisions (but no specific handling of collisions yet)

// NewSimulatedSpace creates a new instance of a simulator with given size and update rate.
func NewSimulatedSpace(x int32, y int32, ticksPerSecond int32) *SimulatedSpace {
	return &SimulatedSpace{
		tree:             quadtree.NewQuadTree(quadtree.BoundingBox{ID: 0, MinX: -x, MaxX: x, MinY: -y, MaxY: y}),
		entities:         make([]RigidBody, 0, 1000),
		fixed:            make([]RigidBody, 0, 1000),
		updatesPerSecond: ticksPerSecond,
	}
}

// SimulatedSpace is struct to hold all state of a single simulator
type SimulatedSpace struct {
	tree     quadtree.QuadTree
	entities []RigidBody // Anything that can collide in the playspace
	fixed    []RigidBody // Anything that can collide but is fixed in place.

	updatesPerSecond int32 // Updates to physics per second.
	lastUpdate       time.Time
	TickID           uint32
}

// AddEntity will add the given entity to the simulation
func (ss *SimulatedSpace) AddEntity(body RigidBody, fixed bool) {
	if fixed {
		ss.fixed = append(ss.fixed, body)
	} else {
		ss.entities = append(ss.entities, body)
	}
	if !ss.tree.Add(body.bounds) {
		panic("failed to add body to space")
	}
}

// RemoveEntity will remove the given entity from the simulation
func (ss *SimulatedSpace) RemoveEntity(body RigidBody, fixed bool) {
	if fixed {
		for cidx, f := range ss.fixed {
			if f.ID == body.ID {
				slen := len(ss.fixed) - 1
				ss.fixed[cidx] = ss.fixed[slen]
				ss.fixed = ss.fixed[:slen]
				break
			}
		}
	} else {
		for cidx, f := range ss.entities {
			if f.ID == body.ID {
				slen := len(ss.entities) - 1
				ss.entities[cidx] = ss.entities[slen]
				ss.entities = ss.entities[:slen]
				break
			}
		}
	}
	ss.tree.Remove(body.bounds)
}

// Tick will advance the state of the simulation by a tick (1 / ss.updatesPerSecond seconds)
func (ss *SimulatedSpace) Tick(sendUpdate bool) []PhysicsEntityUpdate {
	ss.TickID++
	ss.lastUpdate = time.Now()

	var numdirty int
	for i := 0; i < len(ss.entities); i++ {
		changed := false
		// First check for any forces applied
		if ss.entities[i].Force.X != 0 || ss.entities[i].Force.Y != 0 {
			mod := MultVect2(ss.entities[i].Force, ss.entities[i].InvMass/ss.updatesPerSecond)
			ss.entities[i].Velocity.X += mod.X
			ss.entities[i].Velocity.Y += mod.Y
		}
		if ss.entities[i].Torque > 0.0 {
			ss.entities[i].AngularVelocity += (ss.entities[i].Torque * float64(ss.entities[i].InvInertia)) / float64(ss.updatesPerSecond)
		}
		// Now see if we are moving now.
		if ss.entities[i].Velocity.X != 0 {
			ss.entities[i].Position.X += ss.entities[i].Velocity.X / ss.updatesPerSecond
			changed = true
		}
		if ss.entities[i].Velocity.Y != 0 {
			ss.entities[i].Position.Y += ss.entities[i].Velocity.Y / ss.updatesPerSecond
			changed = true
		}
		if ss.entities[i].AngularVelocity != 0.0 {
			ss.entities[i].Angle += ss.entities[i].AngularVelocity / float64(ss.updatesPerSecond)
			for ss.entities[i].Angle > FullCircle {
				ss.entities[i].Angle -= FullCircle
			}
			for ss.entities[i].Angle < -FullCircle {
				ss.entities[i].Angle += FullCircle
			}
			changed = true
		}
		if changed {
			numdirty++
			ss.entities[i].dirty = true
			oldBounds := ss.entities[i].bounds
			// Update bounds
			ss.entities[i].bounds = RigidBounds(ss.entities[i])
			// Have to synchronously move everything, no multithreaded mutation of quadtree right now
			ss.tree.Move(ss.entities[i].bounds, oldBounds)
		}
	}

	// Can read collision data in parallel with quadtree.
	results := make([]PhysicsEntityUpdate, numdirty)
	var ridx int
	var wg sync.WaitGroup
	wg.Add(numdirty)
	for i := 0; i < len(ss.entities); i++ {
		if !ss.entities[i].dirty {
			continue
		}
		ss.entities[i].dirty = false
		go func(index, ridx int) {
			collisions := ss.tree.Query(ss.entities[index].bounds)
			for j := 0; j < len(collisions); j++ {
				if collisions[j].ID == ss.entities[index].ID {
					continue
				}
				results[ridx] = PhysicsEntityUpdate{
					UpdateType: UpdateCollision,
					ObjA:       ss.entities[index].ID,
					ObjB:       collisions[j].ID,
				}
			}
			wg.Done()
		}(i, ridx)
		ridx++
	}
	wg.Wait()

	//cleanup results - not all moved things will intersect
	for i := 0; i < len(results); i++ {
		if results[i].UpdateType == UnknownUpdate {
			rlen := len(results) - 1
			results[i] = results[rlen]
			results = results[:rlen]
		}
	}
	return results
}
